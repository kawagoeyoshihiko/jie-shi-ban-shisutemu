package BBS.beans;

import java.io.Serializable;
import java.util.Date;

public class UserMessage implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String user_id;
    private String bodytext;
    private Date created_date;
    private String category;
    private String title;


	public String getuser_id() {
		return user_id;
	}
	public void setuser_id(String user_id) {
		this.user_id = user_id;
	}
	public String getText() {
		return bodytext;
	}
	public void setText(String bodytext) {
		this.bodytext = bodytext;
	}
	public Date getCreated_date() {
		return created_date;
	}
	public void setCreated_date(Date created_date) {
		this.created_date = created_date;
	}

	public String getCategory() {
		return category;
	}

	public void setCategory(String category) {
		this.category = category;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}

}