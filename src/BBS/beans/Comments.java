package BBS.beans;

import java.io.Serializable;
import java.util.Date;

public class Comments implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String comments;
    private int userId;
    private int Post_id;

    private Date createdDate;
    private Date updatedDate;
    private String loginUser;
	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public Date getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Date createdDate) {
		this.createdDate = createdDate;
	}
	public Date getUpdatedDate() {
		return updatedDate;
	}
	public void setUpdatedDate(Date updatedDate) {
		this.updatedDate = updatedDate;
	}
	public String getComments() {
		return comments;
	}
	public void setComments(String comments) {
		this.comments = comments;
	}
	public int getUserId() {
		return userId;
	}
	public void setUserId(int userId) {
		this.userId = userId;
	}
	public String getPostid() {
		return loginUser;
	}
	public void setPostid(String loginUser) {
		this.loginUser = loginUser;
	}
	public int getPost_id() {
		return Post_id;
	}
	public void setPost_id(int post_id) {
		Post_id = post_id;
	}

}