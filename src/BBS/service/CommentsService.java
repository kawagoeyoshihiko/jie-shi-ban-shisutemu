package BBS.service;

import static BBS.utils.CloseableUtil.*;
import static BBS.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import BBS.beans.Comments;
import BBS.dao.CommentDao;

public class CommentsService {

    public void register(Comments post_id ,Comments comments) {

        Connection connection = null;
        try {
            connection = getConnection();

            CommentDao CommentDao = new CommentDao();
            CommentDao.insert(connection, post_id , comments);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<Comments> getComments() {

        Connection connection = null;
        try {
            connection = getConnection();


            CommentDao CommentDao = new CommentDao();
            List<Comments> ret = CommentDao.getCommetns(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}