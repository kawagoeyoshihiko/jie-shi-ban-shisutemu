package BBS.service;

import static BBS.utils.CloseableUtil.*;
import static BBS.utils.DBUtil.*;

import java.sql.Connection;
import java.util.List;

import BBS.beans.Message;
import BBS.beans.UserMessage;
import BBS.dao.MessageDao;
import BBS.dao.UserMessageDao;

public class MessageService {

    public void register(Message bodytext ,Message user_id,Message title,Message category) {

        Connection connection = null;
        try {
            connection = getConnection();

            MessageDao messageDao = new MessageDao();
            messageDao.insert(connection, bodytext , user_id , title ,  category);

            commit(connection);
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
    private static final int LIMIT_NUM = 1000;

    public List<UserMessage> getMessage() {

        Connection connection = null;
        try {
            connection = getConnection();

            UserMessageDao messageDao = new UserMessageDao();
            List<UserMessage> ret = messageDao.getUserMessages(connection, LIMIT_NUM);

            commit(connection);

            return ret;
        } catch (RuntimeException e) {
            rollback(connection);
            throw e;
        } catch (Error e) {
            rollback(connection);
            throw e;
        } finally {
            close(connection);
        }
    }
}