package BBS.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import BBS.beans.Message;
import BBS.beans.User;
import BBS.service.MessageService;

@WebServlet(urlPatterns = { "/NewMessageServlet" })
public class NewMessageServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;

	@Override
	protected void doPost(HttpServletRequest request,
			HttpServletResponse response) throws IOException, ServletException {


		HttpSession session = request.getSession();

		List<String> messages = new ArrayList<String>();

		if (isValid(request, messages) == true) {
			User user = (User) session.getAttribute("loginUser");

			Message message = new Message();

			message.setText(request.getParameter("bodytext"));
			message.setTitle(request.getParameter("title"));
			message.setCategory(request.getParameter("category"));
			message.setUserId(user.getId());

			new MessageService().register(message, message, message, message);

			response.sendRedirect("./");
		} else {
			session.setAttribute("errorMessages", messages);
			response.sendRedirect("newtext");
		}
	}

	private boolean isValid(HttpServletRequest request, List<String> messages) {
		String category = request.getParameter("category");
		String title = request.getParameter("title");
		String message = request.getParameter("bodytext");
		System.out.println(message);

		if (StringUtils.isEmpty(message) == true) {
			messages.add("メッセージを入力してください");
		}
		if (1000 < message.length()) {
			messages.add("1000文字以下で入力してください");
		}
		if (messages.size() == 0) {
			return true;
		}

		if (StringUtils.isEmpty(title) == true) {
			messages.add("件名を入力してください。");
		}
		if (30 < title.length()) {
			messages.add("30文字以下で入力してください。");
		}
		if (messages.size() == 0) {
			return true;
		}

		if (StringUtils.isEmpty(category) == true) {
			messages.add("カテゴリを入力してください。");
		}
		if (10 < category.length()) {
			messages.add("10文字以下で入力してください。");
		}
		if (messages.size() == 0) {
			return true;
		}

		else {
			return false;
		}
	}

}