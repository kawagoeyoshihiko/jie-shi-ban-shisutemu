package BBS.dao;

import static BBS.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import BBS.beans.User;
import BBS.exception.NoRowsUpdatedRuntimeException;
import BBS.exception.SQLRuntimeException;



public class UserDao {

    public void insert(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO users ( ");
            sql.append("account");
            sql.append(",user_id");
            sql.append(", password");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append("?");
            sql.append(", ?"); //
            sql.append(", ?"); // password
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_datek
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, user.getAccount());
            ps.setString(2, user.getuser_id());
            ps.setString(3, user.getPassword());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public User getUser(Connection connection, String account,String user_id,
            String password) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM  users WHERE user_id = ? AND password = ?";

            ps = connection.prepareStatement(sql);
            ps.setString(1, user_id);
            ps.setString(2, password);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<User> toUserList(ResultSet rs) throws SQLException {

        List<User> ret = new ArrayList<User>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String user_id = rs.getString("user_id");
                String password = rs.getString("password");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Timestamp updatedDate = rs.getTimestamp("updated_date");

                User user = new User();
                user.setId(id);
                user.setuser_id(user_id);
                user.setPassword(password);
                user.setCreatedDate(createdDate);
                user.setUpdatedDate(updatedDate);

                ret.add(user);
            }
            return ret;
        } finally {
            close(rs);
        }
    }
    public User getUser(Connection connection, int id) {

        PreparedStatement ps = null;
        try {
            String sql = "SELECT * FROM users WHERE id = ?";

            ps = connection.prepareStatement(sql);
            ps.setInt(1, id);

            ResultSet rs = ps.executeQuery();
            List<User> userList = toUserList(rs);
            if (userList.isEmpty() == true) {
                return null;
            } else if (2 <= userList.size()) {
                throw new IllegalStateException("2 <= userList.size()");
            } else {
                return userList.get(0);
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }
    public void update(Connection connection, User user) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("UPDATE users SET");
            sql.append("  user_id = ?");
            sql.append(", password = ?");
            sql.append(", updated_date = CURRENT_TIMESTAMP");
            sql.append(" WHERE");
            sql.append(" id = ?");

            ps = connection.prepareStatement(sql.toString());
            ps.setString(1, user.getuser_id());
            ps.setString(2, user.getPassword());
            ps.setInt(3, user.getId());

            int count = ps.executeUpdate();
            if (count == 0) {
                throw new NoRowsUpdatedRuntimeException();
            }
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }

    }

}