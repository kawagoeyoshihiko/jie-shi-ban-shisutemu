package BBS.dao;

import static BBS.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import BBS.beans.Comments;
import BBS.exception.SQLRuntimeException;

public class CommentDao {

    public void insert(Connection connection, Comments loginUser ,Comments comments) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("INSERT INTO comments ( ");
            sql.append("post_id");
            sql.append(", comments");
            sql.append(", created_date");
            sql.append(", updated_date");
            sql.append(") VALUES (");
            sql.append(" ?"); // user_id
            sql.append(", ?");// text
            sql.append(", CURRENT_TIMESTAMP"); // created_date
            sql.append(", CURRENT_TIMESTAMP"); // updated_date
            sql.append(")");

            ps = connection.prepareStatement(sql.toString());
            ps.setInt(1, loginUser.getPost_id());
            ps.setString(2, comments.getComments());
            ps.executeUpdate();
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    public List<Comments> getCommetns(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("comments.id as id, ");
            sql.append("comments.post_id as post_id, ");
            sql.append("comments.comments as comments, ");
            sql.append("comments.created_date as created_date ");
            sql.append("FROM comments ");
//            sql.append("INNER JOIN messages ");
//            sql.append("ON comments.user_id = users.id ");
//       sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<Comments> ret = toComments(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }



    private List<Comments> toComments(ResultSet rs)
            throws SQLException {

        List<Comments> ret = new ArrayList<Comments>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                int post_id = rs.getInt("post_id");
                String comments = rs.getString("comments");
                Timestamp createdDate = rs.getTimestamp("created_date");
                Comments comment = new Comments();
                comment.setId(id);
                comment.setPost_id(post_id);
                comment.setCreatedDate(createdDate);
                comment.setComments(comments);
                ret.add(comment);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}
