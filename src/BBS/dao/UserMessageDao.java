package BBS.dao;

import static BBS.utils.CloseableUtil.*;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import BBS.beans.UserMessage;
import BBS.exception.SQLRuntimeException;

public class UserMessageDao {

    public List<UserMessage> getUserMessages(Connection connection, int num) {

        PreparedStatement ps = null;
        try {
            StringBuilder sql = new StringBuilder();
            sql.append("SELECT ");
            sql.append("messages.id as id, ");
            sql.append("messages.text as text, ");
            sql.append("messages.title as title, ");
            sql.append("messages.category as category, ");
            sql.append("messages.user_id as user_id_1, ");
            sql.append("users.account as user_id, ");
            sql.append("messages.created_date as created_date ");
            sql.append("FROM messages ");
            sql.append("INNER JOIN users ");
            sql.append("ON messages.user_id = users.id ");
            sql.append("ORDER BY created_date DESC limit " + num);

            ps = connection.prepareStatement(sql.toString());

            ResultSet rs = ps.executeQuery();
            List<UserMessage> ret = toUserMessageList(rs);
            return ret;
        } catch (SQLException e) {
            throw new SQLRuntimeException(e);
        } finally {
            close(ps);
        }
    }

    private List<UserMessage> toUserMessageList(ResultSet rs)
            throws SQLException {

        List<UserMessage> ret = new ArrayList<UserMessage>();
        try {
            while (rs.next()) {
            	int id = rs.getInt("id");
                String user_id = rs.getString("user_id");
                String text = rs.getString("text");
                String title = rs.getString("title");
                String category = rs.getString("category");
                Timestamp createdDate = rs.getTimestamp("created_date");
                UserMessage message = new UserMessage();
                message.setId(id);
                message.setuser_id(user_id);
                message.setCreated_date(createdDate);
                message.setText(text);
                message.setTitle(title);
                message.setCategory(category);
                ret.add(message);
            }
            return ret;
        } finally {
            close(rs);
        }
    }

}