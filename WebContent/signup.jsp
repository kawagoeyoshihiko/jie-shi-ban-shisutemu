<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>ユーザー新規登録画面</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
<script type="text/javascript" src="./js/list.js"></script>
</head>
<body>
	<div class="main-contents">
		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>
		<h2>ユーザー新規登録画面</h2>
		<form action="signup" method="post">
			<br /> <label for="user_id">ログインID</label> <input name="user_id"
				id="user_id"
				style="min-width: 170px; min-height: 20px; resize: none; border: solid 3px #6091d3; border-radius: 10px;" /><br />
			<label for="password">パスワード</label> <input name="password"
				type="password" id="password"
				style="min-width: 170px; min-height: 20px; resize: none; border: solid 3px #6091d3; border-radius: 10px;" />
			<br /> <label for="account">名称</label> <input name="account"
				id="account"
				style="min-width: 170px; min-height: 20px; resize: none; border: solid 3px #6091d3; border-radius: 10px;" />
			<br /> <input type="submit" value="登録"> <br> <a
				href="./">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Yoshihiko Kawagoe</div>
	</div>
</body>
</html>