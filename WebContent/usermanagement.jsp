<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>ユーザー管理画面</title>
<script type="text/javascript" src="./js/test.js"></script>
</head>
<body>
	<!-- memo ここに表を作りその中にDBに登録したメンバーの情報を呼び出すforeachタグ -->
	<!-- 下記に作成してるのは枠のたたきのみ  -->
	<table border=1>
		<tr>
			<td>ID</td>
			<td>名称</td>
			<td>ログインID</td>
			<td>アカウント復活</td>
			<td>アカウント停止</td>
		</tr>
		<tr>
			<td>1</td>
			<td>テスト君1</td>
			<td>testID(表示のみ)</td>
			<td><p>
					<input type="button" value="復活" onClick="Resumption()">
				</p></td>
			<td><p>
					<input type="button" value="停止" onClick="disp()">
				</p></td>
		</tr>


	</table>
		<div class="copyright">Copyright(c)Yoshihiko Kawagoe</div>
</body>
</html>