<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>${loginUser.user_id}の設定</title>
<link href="css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="main-contents">

		<c:if test="${ not empty errorMessages }">
			<div class="errorMessages">
				<ul>
					<c:forEach items="${errorMessages}" var="message">
						<li><c:out value="${message}" />
					</c:forEach>
				</ul>
			</div>
			<c:remove var="errorMessages" scope="session" />
		</c:if>

		<form action="usersetting" method="post">
			<br /> <label for="user_id">アカウント名</label>
			<input name="user_id" value="${editUser.user_id}" style="min-width: 170px; min-height: 20px; resize: none; border: solid 3px #6091d3; border-radius: 10px;"/><br /> <label
				for="password">パスワード</label> <input name="password" type="password"
				id="password" style="min-width: 170px; min-height: 20px; resize: none; border: solid 3px #6091d3; border-radius: 10px;"/> <br />
			<br /> <input type="submit" value="登録" /> <br /> <a href="./">戻る</a>
		</form>
		<div class="copyright">Copyright(c)Yoshihiko Kawagoe</div>
	</div>
</body>
</html>