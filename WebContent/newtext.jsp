<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>新規投稿</title>
<script type="text/javascript" src="./js/test.js"></script>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<c:if test="${ not empty errorMessages }">
		<div class="errorMessages">
			<ul>
				<c:forEach items="${errorMessages}" var="message">
					<li><c:out value="${message}" />
				</c:forEach>
			</ul>
		</div>
		<c:remove var="errorMessages" scope="session" />
	</c:if>



	<div class="form-area">
		<c:if test="${ isShowMessageForm }">
			<form action="NewMessageServlet" method="post" name="myFORM">
				<p>
					件名：<br> <input type="text" name="title" size="50"
						style="min-width: 380px; min-height: 20px; margin-left: 15px; resize: none; border: solid 3px #6091d3; border-radius: 10px;">
				</p>

				<p>
					本文：<br>
					<textarea name="bodytext" rows="4" cols="40"></textarea>
				</p>

				<p>
					カテゴリ:<br> <input type="text" name="category" size="50"
						style="min-width: 380px; min-height: 20px; margin-left: 15px; resize: none; border: solid 3px #6091d3; border-radius: 10px;">
				</p>
				<p>
					<input type="submit" value="投稿"><input type="reset"
						value="リセット">
				</p>
				<a href="./">戻る</a>
			</form>
		</c:if>
	</div>
	<div class="copyright">Copyright(c)Yoshihiko Kawagoe</div>
</body>
</html>