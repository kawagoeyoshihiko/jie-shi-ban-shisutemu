window.onload = function() {
	// 今日の日時を表示
	var date = new Date()
	var year = date.getFullYear()
	var month = date.getMonth() + 1
	var day = date.getDate()

	var toTwoDigits = function(num, digit) {
		num += ''
		if (num.length < digit) {
			num = '0' + num
		}
		return num
	}

	var yyyy = toTwoDigits(year, 4)
	var mm = toTwoDigits(month, 2)
	var dd = toTwoDigits(day, 2)
	var ymd = yyyy + "-" + mm + "-" + dd;

	document.getElementById("today").value = ymd;
}
function disp() {

	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if (window.confirm('停止しますか？')) {
		window.alert("停止されました。")

	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else {

		window.alert('キャンセルされました'); // 警告ダイアログを表示

	}
	// 「キャンセル」時の処理終了

}

function Resumption() {

	// 「OK」時の処理開始 ＋ 確認ダイアログの表示
	if (window.confirm('再開しますか？')) {
		window.alert("再開されました。")

	}
	// 「OK」時の処理終了

	// 「キャンセル」時の処理開始
	else {

		window.alert('キャンセルされました'); // 警告ダイアログを表示

	}
	// 「キャンセル」時の処理終了

}

