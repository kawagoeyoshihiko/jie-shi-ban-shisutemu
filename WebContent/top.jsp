<%@page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<%@taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%>
<!DOCTYPE html>
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
<title>掲示板システム</title>
<link href="./css/style.css" rel="stylesheet" type="text/css">
</head>
<body>
	<div class="h1">
		<h1>掲示板システム</h1>
	</div>
	<div class="header">
		<c:if test="${ empty loginUser }">
			<a href="login">ログイン</a>
			<a href="signup">登録する</a>
		</c:if>
		<c:if test="${ not empty loginUser }">
			<a href="./">ホーム</a>
			<a href="usersetting">設定</a>
			<a href="newtext">新規投稿</a>
			<a href="logout">ログアウト</a>
		</c:if>
	</div>
	<c:if test="${ not empty loginUser }">
		<c:forEach items="${messages}" var="message">
			<div class="messages"
				style="background-color: #c9c9d6; width: 420px;">
				<div class="message">
					件名：
					<div class="title">
						<c:out value="${message.title}" />
					</div>
					本文：
					<div class="text">
						<c:out value='${fn:replace(message.text, "
", br)}'
							escapeXml="false" />
					</div>
					カテゴリ：
					<div class="category">
						<c:out value="${message.category}" />
					</div>
					<div class="date">
						投稿日時：
						<fmt:formatDate value="${message.created_date}"
							pattern="yyyy/MM/dd HH:mm:ss" />
					</div>
					<div class="account-name">
						<div class="name">
							投稿者：
							<c:out value="${message.user_id}" />
						</div>
					</div>
					<p>コメント：</p>
					<c:forEach items="${comments}" var="comment">

						<c:if test="${message.id == comment.post_id}">
							<div class="balloon4">
								<p>内容：</p>
									<p><c:out value='${fn:replace(comment.comments, "
", br)}' escapeXml="false" /></p>
									<!--<c:out value="${comment.comments}" />-->
									<br>
								<div class="date">
									コメント日時：
									<c:out value="${comment.createdDate}" />
								</div>
							</div>
						</c:if>
					</c:forEach>
					<c:if test="${ not empty errorMessages }">
						<div class="errorMessages">
							<ul>
								<c:forEach items="${errorMessages}" var="message">
									<li><c:out value="${message}" />
								</c:forEach>
							</ul>
						</div>
						<c:remove var="errorMessages" scope="session" />
					</c:if>
					<form action="NewCommentServlet" method="post" name="myFORM">
						<p>
							<textarea name="comments" rows="4" cols="42" maxlength="500"></textarea>
						</p>
						<input name="post_id" type=hidden value="${message.id}"> <input
							type="submit" value="コメントする" class="button">
					</form>
				</div>
				<br>
			</div>
			<br>
			<br>
		</c:forEach>

	</c:if>
	<div class="copyright">Copyright(c)Yoshihiko Kawagoe</div>
</body>
</html>