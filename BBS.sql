CREATE TABLE Users (
    id          INTEGER       AUTO_INCREMENT PRIMARY KEY,
	account     VARCHAR(10)    NOT NULL,
    user_id     VARCHAR(20)   NOT NULL,
    password    VARCHAR(255)   NOT NULL,
    created_date TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    updated_date TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE messages (
    id          INTEGER     AUTO_INCREMENT PRIMARY KEY,
    user_id     VARCHAR(20)   NOT NULL,
    text        TEXT NOT NULL,
	title     VARCHAR(30)    NOT NULL,
	category     VARCHAR(10)    NOT NULL,
    created_date TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    updated_date TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP
);

CREATE TABLE authors (
    id int(11) PRIMARY KEY NOT NULL,
    name VARCHAR(10),
    kana VARCHAR(20),
	gender VARCHAR(1)
);

DROP TABLE authors;


INSERT INTO authors(
  id,
  name,
  kana,
  gender
) VALUES (
  2,
  '宮沢賢治',
  'みやざわけんじ',
  '男'
), (
  3,
  '芥川龍之介',
  'あくたがわりゅうのすけ',
  '男'
), (
  4,
  '与謝野晶子',
  'よさのあきこ',
  '女'
), (
  5,
  '太宰治',
  'だざいおさむ',
  '男'
);

CREATE DATABASE kawagoe_yoshihiko;

CREATE table users(
id INTEGER AUTO_INCREMENT PRIMARY KEY,
user_id VARCHAR(20)   UNIQUE NOT NULL,
user_pw VARCHAR(20)   UNIQUE NOT NULL,
user_name VARCHAR(10)   UNIQUE NOT NULL
);

drop table users;


SELECT * FROM users account = ? AND password = ?";


SELECT * FROM users user_id = ?,password = ?;


SELECT messages.id as id, messages.text as text, messages.user_id as user_id_1, users.account as user_id, messages.created_date as created_date FROM messages INNER JOIN users ON messages.user_id = users.id ORDER BY created_date DESC limit 1000

TRUNCATE TABLE messages;



CREATE TABLE comments (
    id          INTEGER     AUTO_INCREMENT PRIMARY KEY,
    post_id     VARCHAR(20)   NOT NULL,
    comments        TEXT NOT NULL,
    created_date TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP ,
    updated_date TIMESTAMP      NOT NULL DEFAULT CURRENT_TIMESTAMP
);


SELECT comments.id as id, comments.comments as comments, comments.post_id as post_id_1, messages.id as post_id, comments.created_date as created_date FROM comments INNER JOIN messages ON comments.post_id = messages.id ORDER BY created_date DESC limit 1000


SELECT comments.id as id, comments.post_id as post_id, comments.comments as comments, comments.created_date as created_date FROM comments;

SELECT * FROM comments;
